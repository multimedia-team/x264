#!/bin/sh
# Script modified from upstream source for Debian packaging since packaging
# won't include .git repository.
echo '#define X264_VERSION " r3108 31e19f9"'
echo '#define X264_POINTVER "0.164.3108 31e19f9"'
